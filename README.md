# Kickstarting with Excel

## Overview of Project
The purpose of this project was to introduce us to, and then execute an intermediate understanding of one of the most essential tools for just about every conventional worker in modern society, Excel.  Having given us an expansive worksheet containing a large pool of data concerning Kick-starter campaigns and the degrees to which they were created, succeeded, and funded it was our task to extrapolate information from the worksheet to visualize certain trends and conclusions about specific sectors of the data.  For instance the two specific criteria we were tasked to isolate were how well campaigns did based on the month of the year in which they were launched and how well theatrical play oriented campaigns did based off how much their target goal amount was.  The entire project required an understanding of compressing daunting amounts of the information given to us in more digestible portions utilizing specific excel functions, pivot tables with corresponding filters, and charts that will then help visualize the compressed data into an optimized and immediate graphic.   

## Analysis and Challenges

### Analysis of Outcomes Based on Launch Date
Utilizing our knowledge of pivot tables and filters we constructed a new sheet off the original data pool give nto us titled Theatre Outcomes by Luanch date which can be viewed here. 

### Analysis of Outcomes Based on Goals

### Challenges and Difficulties Encountered

## Results

- What are two conclusions you can draw about the Outcomes based on Launch Date?

- What can you conclude about the Outcomes based on Goals?

- What are some limitations of this dataset?

- What are some other possible tables and/or graphs that we could create?

